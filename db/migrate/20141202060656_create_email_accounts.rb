class CreateEmailAccounts < ActiveRecord::Migration
  def change
    create_table :email_accounts do |t|
      t.string :mailbox_id
      t.references :user, index: true
      t.string :email

      t.timestamps
    end
  end
end
