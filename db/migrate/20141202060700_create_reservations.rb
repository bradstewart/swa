class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.string :confirmation_number
      t.boolean :checked_in
      t.datetime :departs_at
      t.references :user, index: true

      t.timestamps
    end
  end
end
